export interface mnoList{
    id;
    name;
    simboxId;
    uri;
    wsServer: string;
    user;
    password;
}