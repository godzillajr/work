import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from '../service/auth.service';
import * as SIP from 'sip.js/dist/sip';
import { MenuItem} from 'primeng/api';
import { mnoList} from '../models/mnoList';
import { MnolistService} from '../service/mnolist.service';

@Component({
  selector: 'app-webphone',
  templateUrl: './webphone.component.html',
  styleUrls: ['./webphone.component.css']
})
export class WebphoneComponent implements OnInit {
  userAgent: SIP.UA;
  mnos:mnoList[];
  selectedMno: mnoList;
  items: MenuItem[];
  simboxName: string;



  constructor(private mList: MnolistService, private authService:AuthService, private router:Router) { }

  ngOnInit() {
    this.mList.getMnosList().then(data=>this.mnos =data);
    // simboxName = localStorage.getItem('simboxName');
  }

  // onClick(mno){

  //   this.userAgent = new SIP.UA({
  //     uri: mno.uri,
  //     transportOptions: {
  //       wsServers: [mno.wsServer]
  //     },
  //     authorizationUser: mno.user,
  //     password: mno.password,

  //   });
  // }

  onClick(mno){

    this.userAgent = new SIP.UA({
      uri: '6003@192.168.1.25',
      transportOptions: {
        wsServers: ['ws://192.168.1.25:8088/ws']
      },
      authorizationUser: '6003',
      password: '1234',

    });
  }

  // getSimboxName(){
  //   this.simboxName = localStorage.getItem('simboxName');
  // }

  
  configuseragent(callnumber: string) {
    console.log(callnumber)
      this.userAgent.invite('6003@192.168.1.25', {
      sessionDescriptionHandlerOptions: {
          constraints: {
              audio: true,
              video: false

          }

      }
    });
  }

  endcall(){
    SIP.session.terminate();
  }



  chooseSimbox(){
    this.router.navigate(['./navigation']);
  }

  logout(){
    // this.authService.logout().subscribe(success =>{
    //   if (success){
    //     this.router.navigate(['./login']);
    //   }
    // })
    this.authService.logoutWithoutAttemptingRefresh().subscribe();

  }

  button(b){
    (<HTMLInputElement>document.getElementById("target")).value = 
    (<HTMLInputElement>document.getElementById("target")).value + b;
   }
 
   clear_button(uri: string){
    uri = uri.substring(0, uri.length-1);
     (<HTMLInputElement>document.getElementById("target")).value =  uri;
   }

}
