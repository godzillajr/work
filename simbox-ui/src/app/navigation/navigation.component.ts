import { Component, OnInit } from '@angular/core';
import { SimBox } from '../models/SimBox';
import { SimBoxListService } from '../service/sim-box-list.service';
import { MenuItem } from 'primeng/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';




@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
  providers: [MessageService]
})
export class NavigationComponent implements OnInit {

  simBoxes:SimBox[];
  selectedSimBox:SimBox;
  items: MenuItem[];
  private  helper = new JwtHelperService();

  constructor(private http: HttpClient, private authService: AuthService, private sList:SimBoxListService,private messageService: MessageService, private router:Router) { }

  ngOnInit() {
    this.sList.getSimBoxsList().then(data=>this.simBoxes = data);
    localStorage.setItem('simboxId', "0");
  }

  onClick(simbox){
    localStorage.setItem('simboxId',simbox.id);
    // localStorage.setItem('simboxName', simbox.name);
    this.router.navigate(['/webphone']);

  }

  logout(){
    this.authService.logoutWithoutAttemptingRefresh().subscribe();
  }
}
