import { Injectable } from '@angular/core';
import {Router} from '@angular/router';

import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {mnoList} from '../models/mnoList';

@Injectable({
  providedIn: 'root'
})
export class MnolistService {

  constructor(private http: HttpClient, private router:Router) { }

  getMnosList() {
    let simboxId = localStorage.getItem('simboxId');
    console.log (simboxId);
    return this.http.get(`${environment.apiUrl}/MnoList?simboxId=${simboxId}` )
                .toPromise()
                .then(res => <mnoList[]> res)
                .then(data => { 
                  
                  return data;
  });
  
  }
}
