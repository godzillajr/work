import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { SimBox } from '../models/SimBox';

@Injectable({
  providedIn: 'root'
})
export class SimBoxListService {

  constructor(private http: HttpClient, private router:Router) { }


 

  getSimBoxsList() {
    return this.http.get(`${environment.apiUrl}/SimboxList`)
                .toPromise()
                .then(res => <SimBox[]> res)
                .then(data => { 
                  
                  return data; });
}


}
