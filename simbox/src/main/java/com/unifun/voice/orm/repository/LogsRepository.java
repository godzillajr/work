package com.unifun.voice.orm.repository;

import com.unifun.voice.orm.model.Logs;
import lombok.NoArgsConstructor;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@ApplicationScoped
@NoArgsConstructor
public class LogsRepository {
    @Inject
    EntityManager entityManager;

    public void addLog(Logs logs){
        entityManager.persist(logs);
    }

    @Transactional
    public void LogSession(String username, String action) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String datat = dtf.format(now);

        Logs log = new Logs();
        log.setName(username);
        log.setAction(action);
        log.setDatelog(datat);

        addLog(log);

    }
}
