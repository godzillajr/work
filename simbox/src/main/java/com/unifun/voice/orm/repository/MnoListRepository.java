package com.unifun.voice.orm.repository;

import com.unifun.voice.orm.model.MnoListDb;
import lombok.NoArgsConstructor;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
@NoArgsConstructor
public class MnoListRepository {
    @Inject
    private EntityManager entityManager;

    @Transactional
    public List<MnoListDb> getMnoList(int id) throws Exception {
        Query query = entityManager.createQuery("Select m from MnoListDb m WHERE m.simboxId = " + id);
        return query.getResultList();
    }
}
