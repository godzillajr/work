package com.unifun.voice.orm.repository;

import com.unifun.voice.orm.model.SessionTable;
import lombok.NoArgsConstructor;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
@NoArgsConstructor
public class SessionTableRepository {
    @Inject
    EntityManager entityManager;

    @Transactional
    public void addSession(SessionTable sessionTable){
        entityManager.persist(sessionTable);
    }

    @Transactional
    public void removeSession(SessionTable sessionTable){
        sessionTable = fillMissingID(sessionTable);
        try {
            entityManager.remove(entityManager.contains(sessionTable) ? sessionTable : entityManager.merge(sessionTable));
        } catch(java.lang.IllegalArgumentException e) {
            System.out.println("Cannot delete an entity that was already deleted");
        }
    }

    @Transactional
    public List<SessionTable> getSession() throws Exception {
        Query query =  entityManager.createQuery("SELECT s from SessionTable as s");
        return query.getResultList();
    }

    @Transactional
    public List<String> getTokens() throws Exception {
        Query query = entityManager.createQuery("SELECT token FROM SessionTable");
        return query.getResultList();
    }

    private SessionTable fillMissingID(SessionTable st) {
        try {
            List<SessionTable> stl = getSession();
            for(SessionTable tmp: stl) {
                if(tmp.getToken().equals(st.getToken())) {
                    st.setId(tmp.getId());
                    return st;
                }
            }
        } catch(Exception e) {
            System.out.println(e);
            return null;
        }
        return null;
    }

    public SessionTable getNewSession(String ipAddr, String token) {
        SessionTable sessionTable = new SessionTable();
        sessionTable.setIpAddr(ipAddr);
        sessionTable.setToken(token);
        return sessionTable;
    }

    public boolean checkIfTokenIsNotInDB(String token){
        try{
            for (String tt:getTokens()){
                if(token.equals(tt)){
                    return false;
                }
            }
            return true;
        }
        catch (Exception e){
            System.out.println(e);
        }
        return true;
    }


}
