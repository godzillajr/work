package com.unifun.voice.helpers;

public class Constants {
    public static final String OK_RESPONSE = "ok";
    public static final String NOT_OK_RESPONSE = "notok";
    public static final String EXPIRED_RESPONSE = "expired";
}
