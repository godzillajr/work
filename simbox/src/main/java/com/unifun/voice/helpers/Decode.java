package com.unifun.voice.helpers;

import java.util.Base64;

public class Decode {

    public static String decodeBase64(String request) {
        try {
            byte[] requestBytes = request.getBytes("UTF-8");
            byte[] decodedRequest = Base64.getDecoder().decode(requestBytes);
            String result = new String(decodedRequest);
            return result;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

}
