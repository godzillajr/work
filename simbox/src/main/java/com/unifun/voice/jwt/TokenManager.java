package com.unifun.voice.jwt;

import com.unifun.voice.helpers.Constants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.NoArgsConstructor;

import javax.enterprise.context.ApplicationScoped;
import javax.xml.bind.DatatypeConverter;

@ApplicationScoped
@NoArgsConstructor
public class TokenManager {
    public String verifyIfTokenIsValid(String jwt, String secretKey) {
            //This line will throw an exception if it is not a signed JWS (as expected)
            try {
                Claims claims = Jwts.parser()
                        .setSigningKey(DatatypeConverter.parseBase64Binary(secretKey))
                        .parseClaimsJws(jwt).getBody();
                return Constants.OK_RESPONSE;

            } catch (io.jsonwebtoken.security.SignatureException e) {
                return Constants.NOT_OK_RESPONSE;

            } catch (io.jsonwebtoken.ExpiredJwtException e) {
                return Constants.EXPIRED_RESPONSE;
            }
        }
}
