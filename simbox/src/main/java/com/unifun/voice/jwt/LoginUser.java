package com.unifun.voice.jwt;

import com.unifun.voice.helpers.Decode;
import com.unifun.voice.helpers.KeyS;
import com.unifun.voice.model.TokenResponse;
import com.unifun.voice.model.User;
import com.unifun.voice.orm.repository.LogsRepository;
import io.jsonwebtoken.*;

import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

@Path("/login")
@ApplicationScoped
public class LoginUser {
    @Inject
    LogsRepository logsRepository;

    private static Map<String,String> refreshTokens = new HashMap<>();

    public static Map<String,String> getRefreshTokenInstance() {
        if(refreshTokens == null) {
            refreshTokens =  new HashMap<String, String>();
        }
        return refreshTokens;
    }

    @POST
    public String getUserData(String req){
        User user = validateUserLdap(Decode.decodeBase64(req));
        if (user != null) {
            logsRepository.LogSession(user.getUsername(), "login");
            String token = generateToken("AngularFE001","JavaBE001", "Auth-Token", 1000*16*1, user);
            String refreshToken = generateToken("AngularFE001","JavaBE001", "Auth-Token", 1000*45*1, user);
            addRefreshTokenToList(user, refreshToken);
            return tokenResponse(token, refreshToken);
        }
        return "notok";
    }



    public User validateUserLdap (String reqBody) {
        Jsonb jsonb = JsonbBuilder.create();
        User user = jsonb.fromJson(reqBody, User.class);

        try {
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, "ldap://localhost:389");
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, "cn=" + user.getUsername() + ",ou=users,dc=unifun,dc=in");
            env.put(Context.SECURITY_CREDENTIALS, user.getPassword());
            DirContext ctx = new InitialDirContext(env);
            ctx.close();

            return user;
        } catch (Exception e) {
            return null;
        }
    }

    private  String generateToken(String id, String issuer, String subject, long ttlMillis, User user) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(KeyS.SecretKey);
        Key signingKeyS = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(id)
                .setIssuedAt(now)
                .setSubject(subject)
                .setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKeyS);

        //if it has been specified, let's add the expiration, ex: 60 * 1000 * 1 = 1 min
        if (ttlMillis > 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        String token = builder.compact();
        return token;
    }

    private String tokenResponse(String token, String refreshToken){
        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setJwt(token);
        tokenResponse.setRefreshToken(refreshToken);
        Jsonb jsonb1 = JsonbBuilder.create();
        return jsonb1.toJson(tokenResponse);
    }

    private void addRefreshTokenToList(User user, String refreshToken){
        LoginUser.getRefreshTokenInstance().put(user.getUsername(),refreshToken);
    }

}
