package com.unifun.voice.jwt;

import com.unifun.voice.model.ResponseLogout;
import com.unifun.voice.orm.repository.LogsRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/logout")
@ApplicationScoped
public class LogoutUser {

    @Inject
    LogsRepository logsRepository;


    @POST
    public Response logoutUser(String req) {
        Jsonb jsonb = JsonbBuilder.create();
        ResponseLogout responseLogout = jsonb.fromJson(req, ResponseLogout.class);
        logsRepository.LogSession(responseLogout.getUser(), "logout");


        String token = "";
        if(LoginUser.getRefreshTokenInstance().containsValue(token)){
            LoginUser.getRefreshTokenInstance().entrySet()
                    .removeIf(
                            entry -> (token
                            .equals(entry.getValue())));
        }
        return Response.status(Response.Status.NO_CONTENT).entity("Logout").build();
    }

}
