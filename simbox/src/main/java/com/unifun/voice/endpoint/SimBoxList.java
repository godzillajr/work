package com.unifun.voice.endpoint;


import com.unifun.voice.orm.repository.SimboxListRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;


@Path("/SimboxList")
@RequestScoped
public class SimBoxList {
	@Inject
	SimboxListRepository simboxListRepository;
	@GET
	public String get() {
		try {
			return JsonbBuilder.create().toJson(simboxListRepository.getSimboxList());

		} catch (Exception e) {
			return e.toString();
		}
	}

}