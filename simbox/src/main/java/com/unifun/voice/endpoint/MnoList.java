package com.unifun.voice.endpoint;

import com.unifun.voice.orm.repository.MnoListRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/MnoList")
@RequestScoped
public class MnoList {
    @Inject
    MnoListRepository mnoListRepository;

    @GET
    public String get(@QueryParam("simboxId")int id){
        try {
            return JsonbBuilder.create().toJson(mnoListRepository.getMnoList(id));
        } catch (Exception e){
            return e.toString();
        }
    }
}
